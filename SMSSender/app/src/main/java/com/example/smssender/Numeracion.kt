package com.example.smssender

class Numeracion(pddn: String, pdesde: Int, phasta: Int, poperador: String, plocalidad: String) {
    var ddn: String? = pddn
    var desde: Int? = pdesde
    var hasta: Int? = phasta
    var operador: String? = poperador
    var localidad: String? = plocalidad
}